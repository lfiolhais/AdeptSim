extern crate adapt_mem_adept;
#[macro_use]
extern crate clap;
extern crate adept_lib;

use clap::App;

use adept_lib::alu::*;
use adept_lib::mem::{MemStoreOp, Memory};
use adept_lib::register_file::RegisterFile;
use adept_lib::riscv::decoder::Instruction;
use adept_lib::riscv::isa::RV32I;

fn main() {
    let yaml = load_yaml!(concat!(env!("OUT_DIR"), "/main.yaml"));
    let matches = App::from_yaml(yaml).get_matches();

    if let Some(filename) = matches.value_of("input_file") {
        eprintln!("Loading file: {}", filename);
        let mem_data =
            match adapt_mem_adept::get_adept_data(filename, matches.is_present("HexFile")) {
                Ok(chunks) => chunks,
                Err(e) => panic!(e.to_string()),
            };

        let mut my_mem = Box::new(Memory::new());

        for chunk in mem_data {
            let base_address = chunk.get_base_address();
            for offset in 0..(chunk.get_contents_length() >> 2) {
                let actual_offset = offset << 2;
                let address = (base_address as u32) + (actual_offset as u32);
                my_mem.write_data(
                    &MemStoreOp::from(RV32I::SW),
                    address,
                    // This call to unwrap is safe because actual_offset is
                    // guaranteed to be within contents_length
                    chunk.get_word(actual_offset).unwrap(),
                );
            }
        }
        eprintln!("Finished loading memory from elf");

        let mut pc = 0 as u32;
        let mut reg = RegisterFile::new();
        let mut data_mem = Box::new(Memory::new());

        loop {
            let instruction = my_mem.read_pc(pc);
            let fetch = Instruction::new(instruction);
            if !fetch.is_valid() {
                println!("PC :0x{:08x}", pc);
                println!("{}", reg);
                break;
            }
            println!("{}", fetch);
            // Decoded data is translated to signals for ALU
            let rs = reg.read(&fetch);
            let alu_result = alu_ops(rs.0, rs.1, pc, &fetch);
            // Increments PC (or changes value depending on Alu Result and Instruction)
            pc = branch_execution(&fetch, pc, alu_result, rs.0);
            let mem_result = data_mem.mem_ops(&fetch, alu_result as u32, rs.1 as u32);
            reg.write(&fetch, write_back(alu_result, mem_result, &fetch));
        }
    }
}

// Simple MUX of the WB phase
fn write_back(alu: i32, mem: Option<i32>, select: &Instruction) -> i32 {
    if select.get_instr().is_load() {
        mem.unwrap()
    } else {
        alu
    }
}

fn branch_execution(select: &Instruction, pc: u32, alu: i32, reg: i32) -> u32 {
    match select.get_instr().get_instr_op() {
        RV32I::JAL => select.get_imm().unwrap() as u32,
        RV32I::JALR => (select.get_imm().unwrap() + reg) as u32,
        RV32I::BEQ | RV32I::BGE | RV32I::BGEU if alu == 0 => {
            (pc as i32 + select.get_imm().unwrap()) as u32
        }
        RV32I::BNE if alu != 0 => (pc as i32 + select.get_imm().unwrap()) as u32,
        RV32I::BLT | RV32I::BLTU if alu == 1 => (pc as i32 + select.get_imm().unwrap()) as u32,
        _ => pc + 4,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use adept_lib::alu::alu_ops;
    use adept_lib::riscv::decoder::Instruction;

    #[test]
    fn test_bge_on() {
        // FUNC=101 & OP=1100011 -> BGE , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_101_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(10, 5, pc, &decode); // RS1>RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 10);
        assert_eq!(52, pc_new);
    }

    #[test]
    fn test_bge_off() {
        // FUNC=101 & OP=1100011 -> BGE , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_101_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(5, 10, pc, &decode); // RS1<RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 5);
        assert_eq!(54, pc_new);
    }

    #[test]
    fn test_bgeu_on() {
        // FUNC=111 & OP=1100011 -> BGEU , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_111_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(10, 5, pc, &decode); // RS1>RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 10);
        assert_eq!(52, pc_new);
    }

    #[test]
    fn test_bgeu_off() {
        // FUNC=111 & OP=1100011 -> BGEU , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_101_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(5, 10, pc, &decode); // RS1<RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 5);
        assert_eq!(54, pc_new);
    }

    #[test]
    fn test_bgeu_negative_ints() {
        // FUNC=111 & OP=1100011 -> BGEU , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_111_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(1, -5, pc, &decode); // RS1<RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 1);
        assert_eq!(54, pc_new);
    }

    #[test]
    fn test_beq_on() {
        // FUNC=000 & OP=1100011 -> BEQ , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_000_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(10, 10, pc, &decode); // RS1=RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 10);
        assert_eq!(52, pc_new);
    }

    #[test]
    fn test_beq_off() {
        // FUNC=000 & OP=1100011 -> BEQ , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_000_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(10, 9, pc, &decode); // RS1>RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 10);
        assert_eq!(54, pc_new);
    }

    #[test]
    fn test_bne_on() {
        // FUNC=001 & OP=1100011 -> BNE , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_001_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(10, 9, pc, &decode); // RS1!=RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 10);
        assert_eq!(52, pc_new);
    }

    #[test]
    fn test_bne_off() {
        // FUNC=001 & OP=1100011 -> BNE , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_001_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(9, 9, pc, &decode); // RS1=RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 9);
        assert_eq!(54, pc_new);
    }

    #[test]
    fn test_blt_on() {
        // FUNC=100 & OP=1100011 -> BLT , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_100_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(5, 10, pc, &decode); // RS1<RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 5);
        assert_eq!(52, pc_new);
    }

    #[test]
    fn test_blt_off() {
        // FUNC=100 & OP=1100011 -> BLT , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_100_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(10, 5, pc, &decode); // RS1>RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 5);
        assert_eq!(54, pc_new);
    }

    #[test]
    fn test_bltu_on() {
        // FUNC=100 & OP=1100011 -> BLT , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_110_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(5, 10, pc, &decode); // RS1<RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 5);
        assert_eq!(52, pc_new);
    }

    #[test]
    fn test_bltu_off() {
        // FUNC=100 & OP=1100011 -> BLT , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_110_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(10, 5, pc, &decode); // RS1>RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 5);
        assert_eq!(54, pc_new);
    }

    #[test]
    fn test_bltu_negative_ints() {
        // FUNC=100 & OP=1100011 -> BLT , IMM=2, rs1=XX, rs2=XX
        let decode = Instruction::new(0b0000000_00000_00000_110_00010_1100011);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(3, -4, pc, &decode); // RS1<RS2
        let pc_new = branch_execution(&decode, pc, alu_result, 5);
        assert_eq!(52, pc_new);
    }

    #[test]
    fn test_jal() {
        // OP=1101111-> JAL, IMM=2
        let decode = Instruction::new(0b0000000_00010_00000_000_00000_1101111);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(3, -4, pc, &decode); // PC+4
        let pc_new = branch_execution(&decode, pc, alu_result, 5);
        assert_eq!(2, pc_new);
    }

    #[test]
    fn test_jalr() {
        // OP=1101111-> JAL, IMM=0 RS1=XX
        let decode = Instruction::new(0b0000000_00000_00000_000_00000_1100111);
        let pc = 50; // Random Number for Program Counter
        let alu_result = alu_ops(3, -4, pc, &decode); // PC+4
        let pc_new = branch_execution(&decode, pc, alu_result, 5); // Register value=5
        assert_eq!(5, pc_new);
    }

}
